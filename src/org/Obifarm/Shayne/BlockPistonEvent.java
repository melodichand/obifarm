package org.Obifarm.Shayne;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;

public class BlockPistonEvent implements Listener {

	@EventHandler
	public void onBlockPistonEvent(BlockPistonExtendEvent event) {
		Block block = event.getBlock();
		BlockFace facing = event.getDirection();
		Material blockBeingPushed = block.getRelative(facing, 1).getType();
		
		switch(blockBeingPushed){
		case CROPS:
		case NETHER_WARTS:
		case CARROT:
			event.setCancelled(true);
			block.breakNaturally();
			/*Event canceled if the blocks being pushed are the related farming crops.*/
			/*The block is then broken and dropped, denying the ability to cheat the system.*/
			return;
		default:
			return;
		}
		
	}
}
