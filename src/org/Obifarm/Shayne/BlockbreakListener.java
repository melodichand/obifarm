package org.Obifarm.Shayne;
import org.bukkit.event.Listener;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;

public class BlockbreakListener implements Listener {

	private final Consumer lbconsumer;


	public BlockbreakListener(LogBlock logblock) {
	    lbconsumer = logblock.getConsumer();
	}


	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCropBreak(BlockBreakEvent event) {
		Player player;
		ItemStack itemInHand;
		Block block = event.getBlock();
		int amount;
		short durability;
		
		if(event.getPlayer() != null){
			player = (Player) event.getPlayer();
			itemInHand = player.getItemInHand();
			//player.sendMessage(block.getType().toString());
			durability = itemInHand.getDurability();
		}else{
			return;
		}
		//Canceling breaking dirt from under crops.
		if (block.getType().equals(Material.SOIL) || block.getType().equals(Material.SOUL_SAND))
			if (block.getRelative(BlockFace.UP, 1).getType().equals(Material.CROPS)){
				event.setCancelled(true);
				player.sendMessage("You must break the crop before breaking this block.");
			}else if (block.getRelative(BlockFace.UP, 1).getType().equals(Material.CARROT)){
				event.setCancelled(true);
				player.sendMessage("You must break the crop before breaking this block.");
			}else if (block.getRelative(BlockFace.UP, 1).getType().equals(Material.NETHER_WARTS)){
				event.setCancelled(true);
				player.sendMessage("You must break the crop before breaking this block.");
			}
		
		if (!event.isCancelled()){
			switch(itemInHand.getType()){
			//buying gf
			case WOOD_HOE:
			case STONE_HOE:
				amount= 1;
				break;
			case IRON_HOE:
			case GOLD_HOE:
				amount = 2;
				break;
			case DIAMOND_HOE:
				amount = 3;
				break;
			default:
				amount = 0;
				break;
			}
			if (doCalculateDrops(block, amount)){
				event.setCancelled(true);
				lbconsumer.queueBlockBreak(event.getPlayer().getName(), event.getBlock().getState());
				itemInHand.setDurability((short) (durability+1));
				
			}else{
				return;
			}
			
		}
	}


	private boolean doCalculateDrops(Block block, int amount) {
		Material drop;
		Material seed;
		//Switches to find out what block is broken
		switch(block.getType()){
		//Wheat
		case CROPS:
			//Is it fully grown?
			if (block.getData() == 7){
				drop = Material.WHEAT;
				seed = Material.SEEDS;
				block.setType(Material.AIR);
				//DROP PARTY! WORLD 35!
				dropItems(block, amount, drop, seed);
				return true;
			}
		//Netherwarts
		case NETHER_WARTS:
			if (block.getData() == 3){
				drop = Material.NETHER_STALK;
				seed = Material.AIR;
				block.setType(Material.AIR);
				dropItems(block, amount, drop, seed);
				return true;
			}
		//Carrots
		case CARROT:
			if (block.getData() == 7){
				drop = Material.CARROT_ITEM;
				seed = Material.AIR;
				block.setType(Material.AIR);
				dropItems(block, amount, drop, seed);
				return true;
			}
		default:
		}
		return false;
		
	}


	private void dropItems(Block block, int amount, Material drop, Material seed) {
		block.setType(Material.AIR);
		block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(drop, amount));
		//Checking to see if the item dropped and seed are not the same item.
		if(!seed.equals(Material.AIR))
			block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(seed, amount));
		
	}
}
