package org.Obifarm.Shayne;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;
	
	public class ObiFarm extends JavaPlugin{
		private Consumer lbconsumer = null;
		public void onEnable() {
			final PluginManager pm = getServer().getPluginManager();
		    final Plugin plugin = pm.getPlugin("LogBlock");
		    if (plugin != null) 
		    	lbconsumer = ((LogBlock) plugin).getConsumer();
		    
		    LogBlock Logblock = (LogBlock) plugin;
			this.registerListeners((new BlockbreakListener(Logblock)), (new BlockPistonEvent()));
	}
	
	private void registerListeners(Listener... listeners) {
		for (Listener listener : listeners) {
			this.getServer().getPluginManager().registerEvents(listener, this);
		}
	}

}



